// import CartStoreModule from 'Vue/store/modules/cartDecorator/cartStore';
import {
  createApp,
  computed,
  ref,
  onMounted,
} from 'vue';
import { debounce, imageURL } from 'Helpers/utils';
import hugMoneyFormat from 'Vue/filters/hugMoneyFormat';
import { useStore as useCartStore } from 'Vue/store/cart';
import { qs } from 'Helpers/dom/dom';

const CartNotification = createApp({
  delimiters: ['${', '}'],
  setup() {
    const store = useCartStore();
    const cartState = computed(() => (store.state.cartNotificationItems && store.state.cartNotificationItems.items[0]));
    const isActive = ref(false);
    const quantity = ref(0);
    const topBar = qs('top-bar') as TopBar;

    const handleClose = (): void => {
      isActive.value = false;
      topBar.hide();
    };

    /** Debounce update */
    const updateItems = debounce((key, quantityUpdate: number) => {
      store.dispatch('updateCart', { id: key, quantity: quantityUpdate, type: 'updateCartNotification' });
    }, 500);

    const updateCartItem = (evt: string): void => {
      const { key } = store.state.cartNotificationItems.items[0];

      if (evt === 'inc') quantity.value += 1;
      else if (evt === 'dec') quantity.value -= 1;
      else if (evt === 'remove') quantity.value = 0;

      updateItems(key, quantity.value);
    };

    onMounted(() => {
      store.dispatch('fetchCart');
    });

    store.watch((state) => state.cartNotificationItems, () => {
      quantity.value = (store.state.cartNotificationItems.items[0] && store.state.cartNotificationItems.items[0].quantity) || quantity.value;
      if (!isActive.value) {
        isActive.value = true;
        topBar.reveal();
      }
    });

    return {
      cartState,
      isActive,
      quantity,
      handleClose,
      updateCartItem,
      hugMoneyFormat,
      imageURL,
    };
  },
});

CartNotification.mount('.cart-notification');

export default CartNotification;
