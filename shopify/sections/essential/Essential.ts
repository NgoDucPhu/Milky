import Splide from '@splidejs/splide';
import { qsa } from 'Helpers/dom/dom';

declare let Shopify: any;

class EssentialSlide extends HTMLElement {
  private listImage: NodeListOf<HTMLElement>;

  private listThumbs: NodeListOf<HTMLElement>;

  constructor() {
    super();
    this.listImage = qsa('.product-images__wrapper .product-image') as NodeListOf<HTMLElement>;
    this.listThumbs = qsa('.essential-slide__pagination .essential-pagination__item') as NodeListOf<HTMLElement>;
  }

  connectedCallback(): void {
    this.handleSlile();
    Shopify.designMode && document.addEventListener('shopify:section:load', () => {
      this.handleSlile();
    });
  }

  handleSlile(): void {
    const splide = new Splide(this, {
      pagination: false,
      type: 'fade',
      classes: {
        arrows: 'splide__arrows',
        arrow: 'splide__arrow',
        prev: 'splide__arrow--prev',
        next: 'splide__arrow--next',
      },
    });
    let activeThumb: HTMLElement;
    let activeImage: HTMLElement;

    this.listThumbs.forEach((thumb, index) => {
      splide.on('click', () => {
        if (activeThumb !== thumb) {
          if (activeThumb) activeThumb.classList.remove('is-active');

          thumb.classList.add('is-active');
          activeThumb = thumb;
          splide.go(index);
        }
      }, thumb);
    });
    splide.on('mounted move updated', (newIndex) => {
      const thumb = this.listThumbs[newIndex !== undefined ? newIndex : splide.index];
      const image = this.listImage[newIndex !== undefined ? newIndex : splide.index];

      if (thumb && activeThumb !== thumb) {
        if (activeThumb) activeThumb.classList.remove('is-active');
        thumb.classList.add('is-active');
        activeThumb = thumb;
      }

      if (image && activeImage !== image) {
        if (activeImage) activeImage.classList.remove('is-active');
        image.classList.add('is-active');
        activeImage = image;
      }
    });

    splide.mount();
  }
}

customElements.define('essential-slide', EssentialSlide);
