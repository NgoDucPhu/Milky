import PredictiveSearch from '@shopify/theme-predictive-search';
import { useStore as useCartStore } from 'Vue/store/cart';
import { debounce } from 'Helpers/utils';
import { qs, qsa } from 'Helpers/dom/dom';
import { IProductSearch } from 'Types/shopify/product.type';

type TDataResponse = {
  resources: {
    results?: {
      products: IProductSearch[];
    };
  };
};

const renderItem = (product: IProductSearch): string => `<li class='xo-result__item'>
            <a href="${product.url}" class="xo-result__title">
              ${product.title}
            </a>
        </li>`;
class Header {
  headerMenu: NodeListOf<HTMLElement>;

  headerSearchMenu: HTMLElement;

  headerMenuEle: HTMLElement;

  iconExpandMenu: HTMLElement;

  iconCloseMenu: NodeListOf<HTMLElement>;

  menuItemNestedEl: NodeListOf<HTMLElement>;

  iconSearchMob: HTMLElement;

  iconSearchDes: HTMLElement;

  searchInputEle: HTMLElement;

  logInEle: HTMLElement;

  logOutEle: HTMLElement;

  iconBackEle: HTMLElement;

  iconCloseAcc: HTMLElement;

  iconCloseSearchMob: HTMLElement;

  readonly LIMIT_RESULT = 5;

  constructor() {
    this.logInEle = qs('.header__form-customer') as HTMLElement;
    this.iconBackEle = qs('.xo-account__back') as HTMLElement;
    this.logOutEle = qs('.xo-account__logout') as HTMLElement;
    this.iconSearchMob = qs('.header__search') as HTMLElement;
    this.iconSearchDes = qs('.menu__search-icon') as HTMLElement;
    this.searchInputEle = qs('.menu__right-item.icon-search') as HTMLElement;
    this.headerSearchMenu = qs('.header-menu__search') as HTMLElement;
    this.headerMenu = qsa('.header-menu--main') as NodeListOf<HTMLElement>;
    this.headerMenuEle = qs('.xo-header__menu') as HTMLElement;
    this.iconExpandMenu = qs('.icon-toggle-menu') as HTMLElement;
    this.iconCloseMenu = qsa('.icon-close') as NodeListOf<HTMLElement>;
    this.iconCloseAcc = qs('.icon-close-account') as HTMLElement;
    this.iconCloseSearchMob = qs('.icon-close--search') as HTMLElement;
    this.menuItemNestedEl = qsa('.menu__list-child') as NodeListOf<HTMLElement>;
  }

  init(): void {
    this.handleExpandMenu();
    this.handleToggleNestedMenu();
    this.handleExpandSearchInput();
    this.handleItemCount();

    const inputSearchEl = qs('.menu__right-item .menu__search-input') as HTMLElement;
    const resultItemEl = qs('.menu__right-item .xo-result__list') as HTMLElement;
    this.handleSearchResult(inputSearchEl, resultItemEl);

    const inputSearchElMob = qs('.search__form .search_input') as HTMLElement;
    const resultItemElMod = qs('.search__form .xo-result__list') as HTMLElement;
    this.handleSearchResult(inputSearchElMob, resultItemElMod);

    const resetInput = qsa('button[type="reset"]') as NodeListOf<HTMLElement>;
    resetInput.forEach((ele: HTMLElement) => {
      ele.addEventListener('click', () => {
        resultItemEl.innerHTML = '';
        resultItemElMod.innerHTML = '';
        this.searchInputEle.classList.remove('is-active');
      });
    });
  }

  handleItemCount = (): void => {
    const countEl = qsa('.header-cart__item-count span') as NodeListOf<HTMLElement>;
    const store = useCartStore();
    store.watch((state) => state.cartCount, (value) => {
      countEl.forEach((item) => {
        // eslint-disable-next-line no-param-reassign
        item.innerHTML = `${value}`;
      });
    });
  };

  handleExpandMenu(): void {
    this.iconExpandMenu.addEventListener('click', () => {
      this.headerMenu[0].classList.add('is-active');
      document.body.classList.add('remove-scrolling');
    });

    this.iconCloseMenu.forEach((item: HTMLElement) => {
      item.addEventListener('click', () => {
        this.headerMenu.forEach((ele) => {
          ele.classList.remove('is-active');
        });
        document.body.classList.remove('remove-scrolling');
      });
    });

    this.iconSearchMob.addEventListener('click', () => {
      this.headerSearchMenu.classList.add('is-active');
      document.body.classList.add('remove-scrolling');
    });

    this.iconCloseSearchMob.addEventListener('click', () => {
      this.headerSearchMenu.classList.remove('is-active');
      document.body.classList.remove('remove-scrolling');
    });

    this.logInEle?.addEventListener('click', () => {
      this.headerMenuEle.classList.add('is-vertical');
    });

    this.logOutEle.addEventListener('click', () => {
      this.headerMenuEle.classList.remove('is-vertical');
    });

    this.iconBackEle.addEventListener('click', () => {
      this.headerMenuEle.classList.remove('is-vertical');
    });

    this.iconCloseAcc.addEventListener('click', () => {
      this.headerMenu[0].classList.remove('is-active');
      document.body.classList.add('remove-scrolling');
    });
  }

  /**
   * Handle open, collapsed with menu item nested
   */
  handleToggleNestedMenu(): void {
    const expandNestedTitle = qsa('.menu--nested .menu--nested__icon') as NodeListOf<HTMLElement>;
    expandNestedTitle.forEach((item: HTMLElement, index: number) => {
      item.addEventListener('click', () => {
        const ele = this.menuItemNestedEl[index] as HTMLElement;
        const maxHeight = ele.scrollHeight;
        ele.style.setProperty('--max-height-menu', `${maxHeight}px`);
        ele.parentElement?.classList.toggle('is-expand');
      });
    });
  }

  handleExpandSearchInput(): void {
    const resultItemEl = qs('.menu__right-item .xo-result__list') as HTMLElement;
    const inputSearchEl = qs('.menu__right-item .menu__search-input') as HTMLInputElement;
    const topBarEle = qs('.header-wrapper') as HTMLElement;
    this.iconSearchDes.addEventListener('click', () => {
      this.searchInputEle.classList.add('is-active');
      document.body.classList.add('header-search--active');
    });
    const overlay = qs('.overlay') as HTMLElement;
    overlay.addEventListener('click', () => {
      this.searchInputEle.classList.remove('is-active');
      document.body.classList.remove('header-search--active');
      resultItemEl.innerHTML = '';
      inputSearchEl.value = '';
    });
  }

  handleSearchResult(element: HTMLElement, elementWrapper: HTMLElement): void {
    element?.addEventListener('input', debounce((evt) => {
      const query = (<HTMLInputElement>evt.target).value;
      if (query.length === 0) {
        // eslint-disable-next-line no-param-reassign
        elementWrapper.innerHTML = '';
      } else {
        // eslint-disable-next-line no-param-reassign
        elementWrapper.innerHTML = '';
        const predictiveSearch = new PredictiveSearch({
          resources: {
            type: [PredictiveSearch.TYPES.PRODUCT],
            limit: this.LIMIT_RESULT,
            options: {
              unavailable_products: PredictiveSearch.UNAVAILABLE_PRODUCTS.LAST,
              fields: [
                PredictiveSearch.FIELDS.TITLE,
                PredictiveSearch.FIELDS.VENDOR,
                PredictiveSearch.FIELDS.PRODUCT_TYPE,
                PredictiveSearch.FIELDS.VARIANTS_TITLE,
              ],
            },
          },
        });
        predictiveSearch.on('success', (suggestions: TDataResponse) => {
          const productSuggestions = suggestions.resources.results?.products as IProductSearch[];
          if (productSuggestions.length > 0) {
            productSuggestions.forEach((item: IProductSearch) => {
              // eslint-disable-next-line no-param-reassign
              elementWrapper.innerHTML += renderItem(item);
            });
            const resultCount = `<li class="xo-result__count">
                                  <button type="submit" class="result__count">View all</button>
                                </li>`;
            // eslint-disable-next-line no-param-reassign
            elementWrapper.innerHTML += resultCount;
          }
        });
        predictiveSearch.on('error', (error: Error) => {
          console.error('Error message:', error.message);
        });
        predictiveSearch.query(query);
      }
    }, 300));
  }
}

const header = new Header();
header.init();
