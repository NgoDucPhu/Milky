import Splide from '@splidejs/splide';
import { qs } from 'Helpers/dom/dom';

class PopularProductSlider extends HTMLElement {
  splideContainer: HTMLElement;

  constructor() {
    super();
    this.splideContainer = qs('#popularSlide') as HTMLElement;
    this.init();
  }

  init(): void {
    this.handleSlider();
  }

  handleSlider = (): void => {
    const glide = new Splide(this.splideContainer, {
      type: 'slide',
      perPage: 3,
      pagination: false,
      gap: 30,
      breakpoints: {
        576: {
          perPage: 1,
        },
        768: {
          perPage: 2,
        },
        992: {
          perPage: 3,
        },
      },
    });
    glide.mount();
  };
}

customElements.define('popular-product-slider', PopularProductSlider);
