import { qsa } from 'Helpers/dom/dom';
import { onSectionSelected } from 'Helpers/sections/index';

class Blog {
  private listTags: NodeListOf<HTMLElement>;

  constructor() {
    this.listTags = qsa('.main-blog-tag__item') as NodeListOf<HTMLElement>;
  }

  handle(): void {
    const urlStr = window.location.pathname;

    this.listTags.forEach((item) => {
      if (urlStr.includes(`/tagged/${item.dataset.text}`)) {
        item.classList.add('is-active');
      } else if (!urlStr.includes('/tagged')) {
        this.listTags[0].classList.add('is-active');
      }
    });
  }
}

onSectionSelected('.main-blog', () => {
  const blog = new Blog();
  blog.handle();
});
