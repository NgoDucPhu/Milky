import { qs, qsa } from 'Helpers/dom/dom';
import { onSectionSelected } from 'Helpers/sections';

class Login {
  loginConatiner: HTMLElement;

  recoverForm: NodeListOf<HTMLElement>;

  loginForm: HTMLElement;

  constructor() {
    this.loginConatiner = qs('.login') as HTMLElement;
    this.loginForm = qs('.login-recover-form') as HTMLElement;
    this.recoverForm = qsa('.recover__btn') as NodeListOf<HTMLElement>;
  }

  init(): void {
    this.toggleForm();
  }

  toggleForm(): void {
    this.loginForm.addEventListener('click', () => {
      this.loginConatiner.classList.add('is-active');
    });

    this.recoverForm.forEach((item) => {
      item.addEventListener('click', () => {
        this.loginConatiner.classList.remove('is-active');
      });
    });
  }
}

onSectionSelected('.login', () => {
  const login = new Login();
  login.init();
});
