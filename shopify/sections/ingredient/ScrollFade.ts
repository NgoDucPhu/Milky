import { qs, qsa } from 'Helpers/dom/dom';
import { onSectionSelected } from 'Helpers/sections/index';

declare let Shopify: any;
class ScrollFade {
  private boxes: NodeListOf<HTMLElement>;

  constructor() {
    this.boxes = qsa('.box-chat') as NodeListOf<HTMLElement>;
  }

  handleEffect(): void {
    this.boxes.forEach((box, index) => {
      this.createObserver(box, index);
    });
  }

  createObserver = (box: HTMLElement, index: number): void => {
    const target = qs('.section-ingredient') as HTMLElement;
    const options = {
      rootMargin: '0px',
      threshold: 0.5,
    };

    const observer = new IntersectionObserver((entries) => {
      entries.forEach((entry) => {
        if (entry.isIntersecting) {
          setTimeout(() => box.classList.add('box-fade-in'), index * 500);
        }
      });
    }, options);

    observer.observe(target);
  };
}

onSectionSelected('.section-ingredient', () => {
  const scrollFade = new ScrollFade();
  scrollFade.handleEffect();

  Shopify.designMode && document.addEventListener('shopify:section:load', () => {
    const newFade = new ScrollFade();
    newFade.handleEffect();
  });
});
