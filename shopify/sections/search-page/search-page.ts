/* eslint-disable no-param-reassign */
import PredictiveSearch from '@shopify/theme-predictive-search';
import { debounce } from 'Helpers/utils';
import { qs, qsa } from 'Helpers/dom/dom';
import { IProductSearch } from 'Types/shopify/product.type';
import { onSectionSelected } from 'Helpers/sections/index';

type TDataResponse = {
  resources: {
    results?: {
      products: IProductSearch[];
    };
  };
};

const renderItem = (product: IProductSearch): string => `<li class='xo-result__item'>
            <a href="${product.url}" class="xo-result__title">
              ${product.title}
            </a>
        </li>`;
declare const limitResult: number;
declare const number: number;
class SearchPage {
  readonly LIMIT_RESULT = limitResult;

  init(): void {
    const inputSearchEl = qs('.search-template input') as HTMLElement;
    const resultItemEl = qs('.search-template .xo-result__list') as HTMLElement;

    this.handleSearchResult(inputSearchEl, resultItemEl);

    const resetInput = qs('.search-template button[type="reset"]') as HTMLElement;
    resetInput.addEventListener('click', () => {
      resultItemEl.innerHTML = '';
    });
  }

  handleSearchResult(element: HTMLElement, elementWrapper: HTMLElement): void {
    element?.addEventListener('input', debounce((evt) => {
      const query = (<HTMLInputElement>evt.target).value;
      if (query.length === 0) {
        elementWrapper.innerHTML = '';
      } else {
        elementWrapper.innerHTML = '';
        const predictiveSearch = new PredictiveSearch({
          resources: {
            type: [PredictiveSearch.TYPES.PRODUCT],
            limit: this.LIMIT_RESULT,
            options: {
              unavailable_products: PredictiveSearch.UNAVAILABLE_PRODUCTS.LAST,
              fields: [
                PredictiveSearch.FIELDS.TITLE,
                PredictiveSearch.FIELDS.VENDOR,
                PredictiveSearch.FIELDS.PRODUCT_TYPE,
                PredictiveSearch.FIELDS.VARIANTS_TITLE,
              ],
            },
          },
        });

        predictiveSearch.on('success', (suggestions: TDataResponse) => {
          const productSuggestions = suggestions.resources.results?.products as IProductSearch[];
          if (productSuggestions.length > 0) {
            productSuggestions.forEach((item: IProductSearch) => {
              elementWrapper.innerHTML += renderItem(item);
            });

            const resultCount = `<li class="xo-result__count">
                                  <button type="submit" class="result__count">View all</button>
                                </li>`;
            elementWrapper.innerHTML += resultCount;
          }
        });

        predictiveSearch.on('error', (error: Error) => {
          console.error('Error message:', error.message);
        });

        predictiveSearch.query(query);
      }
    }, 300));
  }
}

onSectionSelected('.search-template', () => {
  const searchPage = new SearchPage();
  searchPage.init();
});