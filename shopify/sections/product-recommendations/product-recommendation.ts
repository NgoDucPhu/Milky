import Splide from '@splidejs/splide';

class ProductRecommendation extends HTMLElement {
  constructor() {
    super();
    this.setup();
  }

  setup(): void {
    if (document.querySelector('.product-recommendation__splide')) {
      this.handleSlider();
    }
  }

  handleSlider = (): void => {
    const productSplide = document.querySelector('.product-recommendation__splide') as HTMLElement;
    const productPerView = productSplide.dataset.productPerview as string;

    const glide = new Splide('.product-recommendation__splide', {
      gap: 15,
      perPage: Number(productPerView),
      rewind: true,
      breakpoints: {
        576: {
          perPage: 1.2,
        },
        768: {
          perPage: 2,
        },
        992: {
          perPage: 3,
        },
      },
    });
    glide.mount();
  };
}
customElements.define('product-recommendation', ProductRecommendation);