import Splide from '@splidejs/splide';

/**
 * This is a description of the SliderThumb constructor function.
 * @class
 * @classdesc SliderThumb custom element is slider thumb style 01, product style 1
 */
class SliderThumb extends HTMLElement {
  constructor() {
    super();
  }

  connectedCallback():void {
    this.initSlider();
  }

  initSlider():void {
    const {
      primarySliderId,
      secondarySliderId,
      style,
    } = this.dataset;

    const secondarySlider = new Splide(`#${secondarySliderId}`, {
      gap: 10,
      perPage: 3,
      perMove: 1,
      pagination: false,
      autoWidth: true,
      autoHeight: true,
      rewind: true,
      isNavigation: true,
      arrows: style === '1',
    }).mount();

    const primarySlider = new Splide(`#${primarySliderId}`, {
      type: 'fade',
      width: '100%',
      height: '133%',
      heightRatio: 0.75,
      pagination: false,
      arrows: style === '2',
      cover: true,
      rewind: true,
    }); // do not call mount() here.

    primarySlider.sync(secondarySlider).mount();
  }
}

customElements.define('slider-thumb', SliderThumb);
