import Splide from '@splidejs/splide';
import { qs } from 'Helpers/dom/dom';

declare let Shopify: any;

class ProductSlider extends HTMLElement {
  splideContainer: HTMLElement;

  constructor() {
    super();
    this.splideContainer = qs('.product-slider-container') as HTMLElement;
    this.init();
  }

  init(): void {
    this.handleSlider();

    Shopify.designMode && document.addEventListener('shopify:section:load', () => {
      this.handleSlider();
    });
  }

  handleSlider = (): void => {
    const glide = new Splide(this, {
      type: 'fade',
      perPage: 1,
      pagination: false,
      autoplay: true,
      interval: 8000,
      rewind: true,
    });
    glide.mount();
  };
}

customElements.define('product-slider', ProductSlider);
