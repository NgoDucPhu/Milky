import { createApp, ref } from 'vue';
import { IProduct } from 'Types/shopify/product.type';
import hugMoneyFormat from 'Vue/filters/hugMoneyFormat';
import { onSectionSelected } from 'Helpers/sections';

declare let productJson: IProduct;

const MainProduct = createApp({
  delimiters: ['${', '}'],
  setup() {
    const product = ref(productJson);
    const variantSelected = ref(product.value.variants[0]);
    const quantity = ref(1);

    const handleChangeVariant = (variantID: number): void => {
      const variant = product.value.variants.filter((vrt) => vrt.id === variantID);
      [variantSelected.value] = variant;
    };

    const changeQuantity = (evt: string): void => {
      if (evt === 'inc') quantity.value += 1;
      if (evt === 'dec' && quantity.value > 1) quantity.value -= 1;
    };

    return {
      product,
      variantSelected,
      quantity,
      changeQuantity,
      handleChangeVariant,
      hugMoneyFormat,
    };
  },
});

onSectionSelected('#main-product-container', () => {
  MainProduct.mount('#main-product-container');
});
