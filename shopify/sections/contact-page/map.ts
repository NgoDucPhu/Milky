import { qs, qsa } from 'Helpers/dom/dom';

let map: google.maps.Map;

class Map {
  private mapContactUs: HTMLElement;

  constructor() {
    this.mapContactUs = qs('.map-contact-us') as HTMLElement;
  }

  init(): void {
    this.handleChangeAddress();
  }

  handleEmbedMap = (address: string): void => {
    const options = {
      center: { lat: 38.3460, lng: -0.4907 },
      zoom: 14,
    };
    const googleMapEl = qs('.google-map', this.mapContactUs) as HTMLElement;
    map = new google.maps.Map(googleMapEl, options);

    this.handleGeoCoder(address, map);
  };

  handleChangeAddress = (): void => {
    const mapAddresses = qsa('.map-address__item') as NodeListOf<HTMLElement>;

    if (mapAddresses[0]) {
      mapAddresses[0].classList.add('is-active');
      const { address } = mapAddresses[0].dataset;
      this.handleEmbedMap(address || '');
    }

    mapAddresses.forEach((item) => {
      item.addEventListener('click', (e) => {
        e.preventDefault();
        document.querySelector('.map-address__item.is-active')?.classList.remove('is-active');
        (<HTMLElement>e.target).classList.add('is-active');
        const { address } = item.dataset;
        this.handleEmbedMap(address || '');
      });
    });
  };

  handleGeoCoder = (address: string, mapGoogle: google.maps.Map): void => {
    const geocoder = new google.maps.Geocoder();
    geocoder.geocode({ address }, (result, status) => {
      if (status === 'OK') {
        const { location } = result[0].geometry;
        mapGoogle.setCenter(location);
        this.addMarker(address, location);
      }
    });
  };

  addMarker = (address: string, location: any): void => {
    const marker = new google.maps.Marker({
      position: location,
      map,
    });

    const detailWindow = new google.maps.InfoWindow({ content: `<h2>${address}</h2>` });

    marker.addListener('click', () => {
      detailWindow.open(map, marker);
    });
  };
}

window.initMap = (): void => {
  const newMap = new Map();
  newMap.init();
};

/** Get API */
const API_KEY = qs<HTMLElement>('.map-contact-us')?.getAttribute('data-api-key');
if (API_KEY) {
  /** Create the script tag, set the appropriate attributes */
  const script = document.createElement('script');
  script.src = `https://maps.googleapis.com/maps/api/js?key=${API_KEY}&callback=initMap`;
  script.defer = true;
  script.async = true;
  document.head.appendChild(script);
}
