import {
  computed,
  createApp,
  ref,
} from 'vue';
import { debounce } from 'Helpers/utils';
import { ILineItem } from 'Types/shopify/common.type';
import { ICart } from 'Types/shopify/cart.type';
import hugMoneyFormat from 'Vue/filters/hugMoneyFormat';
import { useStore as useCartStore } from 'Vue/store/cart';
import { onSectionSelected } from 'Helpers/sections';

declare let cartJson: ICart;

const CartPage = createApp({
  delimiters: ['${', '}'],
  setup() {
    const store = useCartStore();
    const cart = ref(cartJson);
    const subTotal = computed(() => {
      let subtotal = 0;
      cart.value.items.forEach((item) => {
        subtotal += item.price * item.quantity;
      });
      return subtotal;
    });
    const getLineItem = (key: string): ILineItem => {
      let lineItem = {} as ILineItem;
      cart.value.items.forEach((item) => {
        if (item.key === key) lineItem = item;
      });

      return lineItem;
    };

    const updateCartNote = debounce(async (evt) => {
      const note = evt.target.value;
      await fetch('/cart/update', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({ note }),
      });
    }, 300);

    const updateItems = debounce((id, quantity) => {
      const payload = {
        id,
        quantity,
      };

      store.dispatch('updateCart', payload);
    }, 500);

    const updateCartItem = (evt: string, key: string): void => {
      if (evt === 'dec' && getLineItem(key).quantity > 1) getLineItem(key).quantity -= 1;
      else if (evt === 'inc') getLineItem(key).quantity += 1;

      updateItems(getLineItem(key).key, getLineItem(key).quantity);
    };

    const handleRemoveItem = (evt: MouseEvent, key: string): void => {
      const removeButton = evt.target as HTMLElement;
      removeButton.classList.add('btn--is-loading');
      updateItems(key, 0);
    };

    store.watch((state) => state.cartState, (value) => {
      cart.value = value;
    });
    return {
      cart,
      subTotal,
      updateCartItem,
      updateItems,
      hugMoneyFormat,
      getLineItem,
      updateCartNote,
      handleRemoveItem,
    };
  },
});

CartPage.mount('#cart-template');
onSectionSelected('#cart-template', () => {
});
