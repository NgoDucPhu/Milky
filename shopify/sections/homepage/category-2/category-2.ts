import Splide from '@splidejs/splide';
import { qs } from 'Helpers/dom/dom';

declare let Shopify: any;

class CategorySlider extends HTMLElement {
  splideContainer: HTMLElement;

  constructor() {
    super();
    this.splideContainer = qs('.category-slider-container') as HTMLElement;
    this.init();
  }

  init(): void {
    this.handleSlider();
    Shopify.designMode && document.addEventListener('shopify:section:load', () => {
      this.handleSlider();
    });
  }

  handleSlider = (): void => {
    const number = this.splideContainer;
    const glide = new Splide(this.splideContainer, {
      type: 'slide',
      pagination: false,
      // gap: 30,
      perPage: Number(number.dataset.number),
      breakpoints: {
        576: {
          perPage: 3,
        },
        768: {
          perPage: 3,
        },
        992: {
          perPage: 3,
        },
      },
    });
    glide.mount();
  };
}

customElements.define('category-slider', CategorySlider);
