/* eslint-disable no-unneeded-ternary */
import Splide from '@splidejs/splide';
import { qs, qsa } from 'Helpers/dom/dom';

declare let Shopify: any;

class HeroSlider extends HTMLElement {
  splideContainer: HTMLElement;

  constructor() {
    super();
    this.splideContainer = qs('.hero-slider-container') as HTMLElement;
    this.init();
  }

  init(): void {
    this.handleSlider();

    Shopify.designMode && document.addEventListener('shopify:section:load', () => {
      this.handleSlider();
    });
  }

  handleSlider = (): void => {
    const slides = qsa('.hero-slider-container .splide__slide') as NodeListOf<HTMLElement>;

    const glide = new Splide(this.splideContainer, {
      type: 'fade',
      isNavigation: false,
      pagination: (slides.length === 1) ? false : true,
      rewind: true,
    });
    glide.mount();
  };
}

customElements.define('hero-slider', HeroSlider);
