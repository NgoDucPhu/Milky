import Splide from '@splidejs/splide';
import { qsa } from 'Helpers/dom/dom';

declare let Shopify: any;

class Testimonial extends HTMLElement {
  constructor() {
    super();
    this.init();
  }

  init(): void {
    this.handleSlider();

    Shopify.designMode && document.addEventListener('shopify:section:load', () => {
      this.handleSlider();
    });
  }

  handleSlider = (): void => {
    const glide = new Splide(this, {
      pagination: false,
      type: 'fade',
      classes: {
        arrows: 'splide__arrows',
        arrow: 'splide__arrow',
        prev: 'splide__arrow--prev',
        next: 'splide__arrow--next',
      },
    });

    const container = qsa('.testimonial') as NodeListOf<HTMLElement>;
    container.forEach((item: HTMLElement, index) => {
      item.classList.add(`testimonial-${index}`);
      glide.on('active moved', () => {
        const listImage = qsa(`.testimonial-${index} .testimonial__img`) as NodeListOf<HTMLElement>;
        const slideImage = listImage[glide.index];
        listImage.forEach((element) => {
          element.classList.remove('is-active');
        });
        slideImage.classList.add('is-active');
      });
    });
    glide.mount();
  };
}

customElements.define('testimonial-section', Testimonial);
