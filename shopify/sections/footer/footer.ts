// import { qs, qsa } from 'Helpers/dom/dom';
// import { onSectionSelected } from 'Helpers/sections/index';

// declare let Shopify: any;
// class Footer {
//   private listBubble: NodeListOf<HTMLElement>;

//   constructor() {
//     this.listBubble = qsa('.footer__image') as NodeListOf<HTMLElement>;
//   }

//   handleEffect(): void {
//     this.listBubble.forEach((box) => {
//       this.createObserver(box);
//     });
//   }

//   createObserver = (box: HTMLElement): void => {
//     const target = qs('.footer') as HTMLElement;
//     const options = {
//       rootMargin: '0px',
//       threshold: 0.6,
//     };

//     const observer = new IntersectionObserver((entries) => {
//       entries.forEach((entry) => {
//         if (entry.isIntersecting) {
//           box.classList.add('box-fade-in');
//         }
//       });
//     }, options);

//     observer.observe(target);
//   };
// }

// onSectionSelected('.footer', () => {
//   const footer = new Footer();
//   footer.handleEffect();

//   Shopify.designMode && document.addEventListener('shopify:section:load', () => {
//     const newFade = new Footer();
//     newFade.handleEffect();
//   });
// });
