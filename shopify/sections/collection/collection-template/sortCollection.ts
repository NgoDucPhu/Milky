import { qs, qsa } from 'Helpers/dom/dom';

import { onSectionSelected } from 'Helpers/sections';

class SortCollection {
  filterEle: HTMLElement;

  facetsWrapper: HTMLElement;

  sortEle: HTMLElement;

  sortWrapper: HTMLElement;

  overlay: HTMLElement;

  constructor() {
    this.filterEle = qs('.filter-toolbar-title') as HTMLElement;
    this.facetsWrapper = qs('.facets__wrapper') as HTMLElement;
    this.sortEle = qs('.sort-toolbar-title') as HTMLElement;
    this.sortWrapper = qs('.sort-toolbar__list') as HTMLElement;
    this.overlay = qs('.filter-overlay') as HTMLElement;
  }

  init() : void {
    const filterArr = [
      {
        ele: this.filterEle,
        wrapper: this.facetsWrapper,
        closeIcon: qs('.filter-close') as HTMLElement,
      },
      {
        ele: this.sortEle,
        wrapper: this.sortWrapper,
        closeIcon: qs('.sort-close') as HTMLElement,
      },
    ];

    this.handleCloseDropdown();

    filterArr.forEach((item) => {
      this.handleFilter(item.ele, item.wrapper, item.closeIcon);
    });

    this.handleTag();

    window.addEventListener('DOMContentLoaded', this.handleCheck);
    window.addEventListener('keydown', this.handleEscape);
  }

  onCloseDropdown(): void {
    this.overlay.classList.remove('is-active');
    this.sortWrapper.classList.remove('is-active');
    document.body.classList.remove('remove-scroll');
  }

  handleFilter(ele: HTMLElement, wrapper: HTMLElement, closeIcon: HTMLElement): void {
    ele.addEventListener('click', () => {
      wrapper.classList.toggle('is-active');
      this.overlay.classList.add('is-active');
      document.body.classList.add('remove-scroll');
    });

    closeIcon.addEventListener('click', () => {
      wrapper.classList.toggle('is-active');
      this.overlay.classList.remove('is-active');
      document.body.classList.remove('remove-scroll');
    });
  }

  handleCloseDropdown(): void {
    this.overlay.addEventListener('click', () => {
      this.facetsWrapper.classList.remove('is-active');
      this.onCloseDropdown();
    });

    const sortItem = qsa('.facet-radio');
    sortItem.forEach((item) => {
      item.addEventListener('click', () => {
        this.onCloseDropdown();
      });
    });
  }

  /**
   * Sorting product when page loaded
   */
  handleCheck = (): void => {
    const sortInputList = qsa('.sort-input') as NodeListOf<HTMLInputElement>;
    sortInputList.forEach((item: HTMLInputElement) => {
      if (item.getAttribute('value') === 'best-selling') {
        // eslint-disable-next-line no-param-reassign
        item.checked = true;
      }
    });
  };

  handleEscape = (e: any): void => {
    if (e.keyCode === 27) {
      this.onCloseDropdown();
      this.facetsWrapper.classList.remove('is-active');
    }
  };

  handleTag = (): void => {
    const tagList = qsa('.filter-tag__item') as NodeListOf<HTMLElement>;
    tagList.forEach((item: HTMLElement) => {
      const tag = item.getAttribute('href');

      if (window.location.pathname === tag) {
        item.classList.add('is-active');
      } else {
        item.classList.remove('is-active');
      }
    });
  };
}

onSectionSelected('.collection-template-container', () => {
  const sortCollection = new SortCollection();
  sortCollection.init();
});
