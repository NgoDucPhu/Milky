import { qsa, qs } from 'Helpers/dom/dom';
import { onSectionSelected } from 'Helpers/sections/index';

class Article {
  private sortItem: NodeListOf<HTMLElement>;

  private btn: HTMLElement;

  private iconClose: HTMLElement;

  private form: HTMLElement;

  constructor() {
    this.sortItem = qsa('.article-template-sort__item') as NodeListOf<HTMLElement>;
    this.btn = qs('.article-template__btn-toggle div') as HTMLElement;
    this.iconClose = qs('.article-template__btn-toggle span') as HTMLElement;
    this.form = qs('.article-template__comment-form') as HTMLElement;
  }

  init(): void {
    // this.handleSort();
    this.handleShowComment();
  }

  // handleSort(): void {
  //   this.sortItem.forEach((item, index: number) => {
  //     if (index === 0) {
  //       this.filter(item, index);
  //     }
  //     item.addEventListener('click', () => {
  //       this.filter(item, index);
  //     });
  //   });
  // }

  // filter(item: HTMLElement, index: number): void {
  //   this.sortItem.forEach((ele) => {
  //     ele.classList.remove('is-active');
  //   });
  //   item.classList.add('is-active');
  //   const itemData = item.dataset?.sort as string;
  //   const title = qs('.article-template-sort__text') as HTMLElement;
  //   title.innerHTML = itemData;

  //   const commentDefault = qs('.article-template__comments-default') as HTMLElement;
  //   const commentReverse = qs('.article-template__comments-reverse') as HTMLElement;
  //   if (!index) {
  //     commentDefault.style.display = 'block';
  //     commentReverse.style.display = 'none';
  //   } else {
  //     commentDefault.style.display = 'none';
  //     commentReverse.style.display = 'block';
  //   }
  // }

  handleShowComment(): void {
    const toggleClass = qs('.article-template__btn-toggle') as HTMLElement;
    this.btn.addEventListener('click', () => {
      toggleClass.classList.toggle('is-active');

      const maxHeight = this.form.scrollHeight;
      this.form.style.setProperty('--max-height-answer', `${maxHeight}px`);
      this.form.classList.add('is-active');
    });

    this.iconClose.addEventListener('click', () => {
      toggleClass.classList.remove('is-active');
      this.form.classList.remove('is-active');
    });
  }
}

onSectionSelected('.article-template', () => {
  const article = new Article();
  article.init();
});
