import Splide from '@splidejs/splide';
import { qs } from 'Helpers/dom/dom';

class GridProductSlider extends HTMLElement {
  splideContainer: HTMLElement;

  constructor() {
    super();
    this.splideContainer = qs('.grid-product__left') as HTMLElement;
    this.init();
  }

  init(): void {
    this.handleSlider();
  }

  handleSlider = (): void => {
    const glide = new Splide(this, {
      type: 'slide',
      perPage: 1,
      pagination: false,
      autoplay: true,
      interval: 8000,
      rewind: true,
      gap: 20,
    });

    glide.mount();
  };
}

customElements.define('grid-product-slider', GridProductSlider);
