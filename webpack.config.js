const { merge } = require('webpack-merge');
const { xoCommonConfig } = require('./build-utils/webpack.common');

const addonsFn = (addonsArg) => {
  const addons2 = addonsArg.split(',');
  return addons2.map((addonName) => require(`./build-utils/addons/webpack.${addonName}.js`));
};

const allConfigs = (env) => {
  console.log(env)
  const { mode, addons } = env;
  const envConfig = require(`./build-utils/webpack.${mode}.js`);
  let allConfig = merge(xoCommonConfig, envConfig);
  if (addons) {
    allConfig = merge(xoCommonConfig, envConfig, ...addonsFn(addons));
  }

  return allConfig;
}
module.exports = allConfigs;
