// const path = require('path');
const commonPath = require('./common-path');
const { VueLoaderPlugin } = require('vue-loader')
// const StyleLintPlugin = require('stylelint-webpack-plugin');
// const copyPlugin = require('./common-plugins/copy-plugin');
// const replacePlugin = require('./common-plugins/replace-in-file-plugin');
const webpack = require('webpack')

const xoCommonConfig = {
  name: 'ShopiyThemeStarter',
  entry: './src/index.ts',
  output: {
    path: commonPath.outputPath,
    filename: 'assets/app.js',
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        use: [
          {
            loader: 'babel-loader',
            options: {
              presets: ['@babel/preset-env'],
                plugins: [
                  "transform-custom-element-classes",
                  "transform-es2015-classes",
                  "@babel/plugin-proposal-class-properties",
                ]
            }
          },
          'webpack-import-glob-loader' /** @see https://www.npmjs.com/package/import-glob-loader */
        ]
      },
      {
        test: /\.ts?$/,
        loader: 'ts-loader',
        exclude: /node_modules/,
        options: {
          appendTsSuffixTo: [/\.vue$/],
        },
      },
      {
        test: /\.vue$/,
        loader: 'vue-loader',
        options: {
          loaders: {
            'scss': 'vue-style-loader!css-loader!sass-loader',
            'sass': 'vue-style-loader!css-loader!sass-loader?indentedSyntax',
          },
        },
      },
      {
        test: /\.(s*)css$/,
        use: [
          {
            loader: 'css-loader',
            options: {
              importLoaders: 2,
              sourceMap: false,
            },
          },
          {
            loader: 'postcss-loader',
            options: {
              postcssOptions: {
                plugins: ['autoprefixer'],
                sourceMap: false,
              }
            },
          },
          {
            loader: 'sass-loader',
            options: {
              sourceMap: false,
            },
          },
          'webpack-import-glob-loader' /** @see https://www.npmjs.com/package/import-glob-loader */
        ],
      },
      // {
      //   test: /\.(jpe?g|png|gif|woff|woff2|eot|ttf|svg)(\?[a-z0-9=.]+)?$/,
      //   loader: 'url-loader?limit=100000'
      // }
    ],
  },
  plugins: [
    new webpack.DefinePlugin({
      __VUE_OPTIONS_API__: false,
      __VUE_PROD_DEVTOOLS__: false,
    }),
    new VueLoaderPlugin(),
    // new StyleLintPlugin({
    //   configFile: '.stylelintrc',
    //   context: 'src',
    //   files: '**/*.(s(c|a)ss|css)',
    //   failOnError: false,
    //   quiet: false,
    //   emitErrors: true
    // }),
    // copyPlugin.huwngCopyPlugin,
    // replacePlugin.huwngReplacePlugin,
  ],
  resolve: {
    extensions: ['.vue', '.ts', '.js', '.json'],
    alias: {
      vue: 'vue/dist/vue.esm-bundler.js',
      Components: commonPath.componentsPath,
      Helpers: commonPath.helpersPath,
      Styles: commonPath.stylesPath,
      Shopify: commonPath.themeDevPath,
      Types: commonPath.typesPath,
      Vue: commonPath.vuePath,
     }
  },
  stats: {
    entrypoints: false,
    children: false,
  }
};

module.exports = { xoCommonConfig }
