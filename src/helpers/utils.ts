// eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types
// eslint-disable-next-line @typescript-eslint/explicit-function-return-type
export const debounce = <F extends (...args: any) => any>(func: F, waitFor: number) => {
  let timeout = 0;

  const debounced = (...args: any): void => {
    clearTimeout(timeout);
    timeout = setTimeout(() => func(...args), waitFor);
  };

  return debounced as (...args: Parameters<F>) => ReturnType<F>;
};

export const imageURL = (url: string, thumbSize: string, type = ''): string => {
  let newURL = url || '';
  if (url.includes('cdn.shopify.com')) {
    /** Nếu giá trị truyền vào là url thay vì object ảnh */
    newURL = url
      .replace('.jpg', `_${thumbSize}${type}.jpg`)
      .replace('.JPG', `_${thumbSize}${type}.JPG`)
      .replace('.png', `_${thumbSize}${type}.png`)
      .replace('.PNG', `_${thumbSize}${type}.PNG`)
      .replace('.jpeg', `_${thumbSize}${type}.jpeg`)
      .replace('.JPEG', `_${thumbSize}${type}.JPEG`);
  }
  return newURL;
};
