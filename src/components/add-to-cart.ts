import { useStore as useCartStore } from 'Vue/store/cart';

class AddToCart extends HTMLElement {
  variantId: number;

  quantity: number;

  constructor() {
    super();
  }

  connectedCallback():void {
    this.setup();
  }

  setup(): void {
    this.addEventListener('click', (e) => this.handleAddToCart(e));
  }

  handleAddToCart(evt: MouseEvent): void {
    evt.preventDefault();
    this.updateStore();
  }

  updateStore(): void {
    const {
      variantId,
      quantity,
    } = this.dataset;

    const payload = {
      id: variantId,
      quantity,
    };

    const store = useCartStore();

    this.classList.add('btn--is-loading');
    store.dispatch('addToCart', payload);

    store.watch(() => store.state.isLoading, (oldValue, newValue) => {
      if (!oldValue && newValue) this.classList.remove('btn--is-loading');
    });
  }
}

customElements.define('add-to-cart', AddToCart);
