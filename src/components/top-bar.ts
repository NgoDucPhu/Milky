// eslint-disable-next-line max-classes-per-file

class TopBar extends HTMLElement {
  // lastScroll = 0;
  header: HTMLElement;

  topBarEle: HTMLElement;

  menuItems: NodeListOf<HTMLElement>;

  constructor() {
    super();
    this.header = document.querySelector('.header-wrapper') as HTMLElement;
    this.menuItems = document.querySelectorAll('.xo-link-item') as NodeListOf<HTMLElement>;

    if (document.querySelector('.header-wrapper.sticky-header')) {
      this.topBarEle = document.querySelector('.header-wrapper.sticky-header') as HTMLElement;
    }

    this.handleScroll();
    this.onActiveMenu();

    if (document.querySelector('#shopify-section-header-1')) {
      const shopifySectionHeader = document.querySelector('#shopify-section-header-1') as HTMLElement;
      this.handleUrl(shopifySectionHeader);
    }

    if (document.querySelector('#shopify-section-header-2')) {
      const shopifySectionHeader2 = document.querySelector('#shopify-section-header-2') as HTMLElement;
      this.handleUrl(shopifySectionHeader2);
    }

    if (document.querySelector('#shopify-section-header-3')) {
      const shopifySectionHeader3 = document.querySelector('#shopify-section-header-3') as HTMLElement;
      this.handleUrl(shopifySectionHeader3);
    }
  }

  handleScroll = (): void => {
    let prevScrollpos = 0;

    window.onscroll = (): void => {
      const currentScrollPos = window.pageYOffset || document.documentElement.scrollTop;

      if (document.querySelector('.header-wrapper.sticky-header')) {
        if (currentScrollPos === 0) {
          this.topBarEle.classList.remove('header-wrapper--fixed');
        } else if (currentScrollPos >= 200) {
          this.topBarEle.classList.add('header-wrapper--fixed');
        }

        if (prevScrollpos > currentScrollPos) {
          this.topBarEle.style.top = '0';
        } else if (currentScrollPos > 100) {
          this.topBarEle.style.top = '-120px';
        }

        prevScrollpos = currentScrollPos;
      }
    };
  };

  handleUrl = (param: HTMLElement): void => {
    let localizationIsoCode = '';

    if (document.querySelector('.localization-language')) {
      const localizationEle = document.querySelector('.localization-language') as HTMLElement;
      localizationIsoCode = localizationEle.dataset.isoCode as string;
    }

    if ((window.location.pathname !== '/' && window.location.pathname !== `/${localizationIsoCode}`) && !window.location.pathname.includes('account')) {
      this.header.classList.add('header-wrapper--another');
      // eslint-disable-next-line no-param-reassign
      param.style.height = `${this.header.offsetHeight + 8}px`;
    }

    if (window.location.pathname.includes('products')) {
      param.classList.add('remove-fixed');
    }
  };

  reveal(): void {
    if (document.querySelector('.header-wrapper.sticky-header')) {
      this.topBarEle.classList.add('header-fixed');
    }

    this.header.classList.add('header-fixed');
  }

  hide(): void {
    if (document.querySelector('.header-wrapper.sticky-header')) {
      this.topBarEle.classList.remove('header-fixed');
    }

    this.header.classList.remove('header-fixed');
  }

  onActiveMenu(): void {
    this.menuItems.forEach((item: HTMLElement, index) => {
      const itemHref = item.getAttribute('href');
      const itemHrefArr = item.getAttribute('href')?.split('/');

      if (window.location.pathname === itemHref) {
        this.menuItems[index].classList.add('menu-item--active');
      }

      itemHrefArr?.filter((ele) => ele !== '').forEach((ele) => {
        if (window.location.pathname.includes(ele)) {
          this.menuItems[index].classList.add('menu-item--active');
        } else {
          this.menuItems[index].classList.remove('menu-item--active');
        }
      });
    });
  }
}

customElements.define('top-bar', TopBar);

class HeaderMenu extends HTMLElement {
  constructor() {
    super();
    document.addEventListener('keydown', this.handleEsc);
  }

  handleEsc = (e: KeyboardEvent): void => {
    if (e.keyCode === 27) {
      document.body.classList.remove('remove-scrolling');
      document.querySelectorAll('.header-menu')?.forEach((item: Element) => {
        item.classList.remove('is-active');
      });
    }
  };
}

customElements.define('header-menu', HeaderMenu);