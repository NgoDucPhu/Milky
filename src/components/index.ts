import './top-bar';
import './add-to-cart';
import './product-accordion';
import './product-form';
import './input-number';
import './main-product';
