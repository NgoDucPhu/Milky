import { qsa } from 'Helpers/dom/dom';

class ProductAccordion extends HTMLElement {
  private faqQuestions: NodeListOf<HTMLElement>;

  private faqAnswers: NodeListOf<HTMLElement>;

  constructor() {
    super();
    this.faqQuestions = qsa('.product-accordion__question') as NodeListOf<HTMLElement>;
    this.faqAnswers = qsa('.product-accordion__answer') as NodeListOf<HTMLElement>;
    this.handle();
  }

  handle = (): void => {
    this.faqQuestions?.forEach((item, index) => {
      if (index === 0) {
        this.showItem(item, index);
      }

      item.addEventListener('click', () => {
        this.showItem(item, index);
      });
    });
  };

  showItem = (item: HTMLElement, index: number): void => {
    const ele = this.faqAnswers[index] as HTMLElement;
    const maxHeight = ele.scrollHeight;
    ele.style.setProperty('--max-height-answer', `${maxHeight}px`);
    ele.parentElement?.classList.toggle('is-expand');
  };
}

customElements.define('product-accordion', ProductAccordion);