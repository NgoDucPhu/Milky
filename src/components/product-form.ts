// import { IProduct } from 'Types/shopify/product.type';

// declare let Shopify: any;
// declare let product: IProduct;

// class ProductForm extends HTMLElement {
//   form: HTMLElement;

//   btnAddToCart: HTMLElement;

//   constructor() {
//     super();
//     this.form = this.querySelector('form') as HTMLElement;
//     this.btnAddToCart = this.querySelector('button[type="submit"].product-form__submit') as HTMLElement;
//   }

//   connectedCallback(): void {
//     this.setup();
//   }

//   setup(): void {
//     this.onChangeOptions();
//   }

//   onChangeOptions(): void {
//     const selectCallback = (variant: any): void => {
//       const {
//         id,
//         available,
//       } = variant;

//       this.updateIdAddToCart(id);
//       this.updateAvailableAddToCart(available);
//     };

//     const optionSelector = new Shopify.OptionSelectors('product-select', {
//       product,
//       onVariantSelected: selectCallback,
//       enableHistoryState: true,
//     });
//   }

//   updateIdAddToCart(newValue: string): void {
//     this.btnAddToCart.setAttribute('data-variant-id', newValue);
//   }

//   updateAvailableAddToCart(available: boolean): void {
//     if (available) {
//       this.btnAddToCart.classList.remove('btn--is-soldout');
//     } else {
//       this.btnAddToCart.classList.add('btn--is-soldout');
//     }
//   }
// }

// customElements.define('product-form', ProductForm);
