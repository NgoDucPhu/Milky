// class InputNumber extends HTMLElement {
//   inputWrapperEl: HTMLElement;

//   btnInc: HTMLElement;

//   btnDec: HTMLElement;

//   btnAddToCart: HTMLElement;

//   input: HTMLInputElement;

//   constructor() {
//     super();
//     this.inputWrapperEl = this.querySelector('.input-number-wrapper') as HTMLElement;
//     this.btnAddToCart = document.querySelector('button[type="submit"].product-form__submit') as HTMLElement;
//     this.btnInc = this.querySelector('.product-form__inc') as HTMLElement;
//     this.btnDec = this.querySelector('.product-form__dec') as HTMLElement;
//     this.input = this.querySelector('input') as HTMLInputElement;
//   }

//   connectedCallback(): void {
//     this.setup();
//   }

//   setup():void {
//     this.btnInc.addEventListener('click', (e: Event) => {
//       e.preventDefault();
//       this.input.value = `${Number(this.input.value) + 1}`;
//       this.updateQuantityAddToCart(this.input.value);
//     });

//     this.btnDec.addEventListener('click', (e: Event) => {
//       e.preventDefault();
//       if (Number(this.input.value) > 1) {
//         this.input.value = `${Number(this.input.value) - 1}`;
//         this.updateQuantityAddToCart(this.input.value);
//       }
//     });
//   }

//   updateQuantityAddToCart(newValue: string): void {
//     this.btnAddToCart.setAttribute('data-quantity', newValue);
//   }
// }

// customElements.define('input-number', InputNumber);
