// eslint-disable-next-line @typescript-eslint/ban-ts-comment
// @ts-nocheck
import Vue, { createApp } from 'vue';
import 'Vue/filters';
import store from './vue/store';

/**
 * SCSS
 */
import '@splidejs/splide/dist/css/splide.min.css';
import './styles/main.scss';

/**
 * TS
 */
import './helpers';
import './components';

/**
 * Auto find and import all .ts file in Shopify folder
 */
const tsFiles = require.context('Shopify/', true, /\.ts$/);
tsFiles.keys().forEach(tsFiles);

/**
 * All SECTION is vue instance ( template vue )
 *
 * Properly render vue components inside sections on user insert in the theme editor
 * add the 'vue' keyword to the section's wrapper classes e.g.:
 *
 * {% schema %}
 * {
 *   "class": "vue-section"
 * }
 * {% endschema %}
 */

/* If merchant in designMode */
Shopify.designMode && document.addEventListener('shopify:section:load', (event) => {
  if (event.target.classList.value.includes('vue-section')) {
    const app = createApp({
      delimiters: ['${', '}'],
      store,
    });

    vueComponents.keys().forEach((key) => {
      const component = vueComponents(key).default;
      app.component(component.name, component);
    });

    app.mount(event.target);
  }
});

/* If merchant in normalMode ( is Section ) */
document.querySelectorAll('.shopify-section').forEach((section) => {
  if (section.classList.value.includes('vue-section')) {
    const app = createApp({
      delimiters: ['${', '}'],
      store,
    });

    vueComponents.keys().forEach((key) => {
      const component = vueComponents(key).default;
      app.component(component.name, component);
    });

    app.mount(section);
  }
});

/** If vue instace != section */
document.querySelectorAll('[data-vue-instance]').forEach((element) => {
  const newInstanceVue = createApp({
    store,
  });
  vueComponents.keys().forEach((key) => {
    const component = vueComponents(key).default;
    newInstanceVue.component(component.name, component);
  });

  newInstanceVue.mount(element)
});

console.log('kmacoders developing..');
