// store.ts
import { InjectionKey } from 'vue';
import { createStore, Store } from 'vuex';
import { ILineItem } from 'Types/shopify/common.type';
import { ICart } from 'Types/shopify/cart.type';

export type CartItems = {
  items: ILineItem[]
};

export enum CartStoreMutationTypes {
  SET_CART_STATE = 'SET_CART_STATE',
  SET_CART_NOTIFICATION_ITEM = 'SET_CART_NOTIFICATION_ITEM',
  SET_CART_COUNT = 'SET_CART_COUNT',
  SET_IS_LOADING = 'SET_IS_LOADING',
}

// define your typings for the store state
export interface State {
  cartNotificationItems: CartItems;
  cartState: ICart;
  cartCount: number;
  isLoading: boolean;
}

// define injection key
// eslint-disable-next-line symbol-description
export const key: InjectionKey<Store<State>> = Symbol();

export const store = createStore<State>({
  state: {
    cartNotificationItems: { items: [] },
    cartState: {} as ICart,
    cartCount: 0,
    isLoading: false,
  },
  mutations: {
    [CartStoreMutationTypes.SET_CART_STATE](state: State, payload: ICart): void {
      state.cartState = payload;
      state.cartCount = payload.item_count;
    },
    [CartStoreMutationTypes.SET_CART_NOTIFICATION_ITEM](state: State, payload: CartItems): void {
      state.cartNotificationItems = payload;
      state.cartCount += 1;
    },
    [CartStoreMutationTypes.SET_IS_LOADING](state: State, payload: boolean): void {
      state.isLoading = payload;
    },
    [CartStoreMutationTypes.SET_CART_COUNT](state: State, payload: number): void {
      state.cartCount = payload;
    },
  },
  actions: {
    async addToCart(ctx, payload): Promise<void> {
      const formData = { ...payload };

      try {
        ctx.commit(CartStoreMutationTypes.SET_IS_LOADING, true);
        const res = await fetch('/cart/add.js', {
          method: 'POST',
          headers: {
            'Content-Type': 'application/json',
          },
          body: JSON.stringify(formData),
        });

        if (res.status === 200) {
          const cartState = await res.json();
          ctx.commit(CartStoreMutationTypes.SET_CART_NOTIFICATION_ITEM, { items: [cartState] });
        } else throw Error('Error');
      } catch (error) {
        console.log(error);
      } finally {
        ctx.commit(CartStoreMutationTypes.SET_IS_LOADING, false);
      }
    },
    async updateCart(ctx, payload): Promise<void> {
      const { id, quantity, type } = payload;
      const formData = {
        id,
        quantity,
      };

      // ctx.commit(CartStoreMutationTypes.SET_CART_COUNT, this.state.cartCount += quantity);

      try {
        ctx.commit(CartStoreMutationTypes.SET_IS_LOADING, true);
        const res = await fetch('/cart/change.js', {
          method: 'POST',
          headers: {
            'Content-Type': 'application/json',
          },
          body: JSON.stringify(formData),
        });

        if (res.status === 200) {
          const cartState = await res.json() as ICart;

          if (type === 'updateCartNotification') {
            const cartItem = { items: cartState.items.filter((item) => item.key === id) as ILineItem[] };
            ctx.commit(CartStoreMutationTypes.SET_CART_NOTIFICATION_ITEM, cartItem);
          }
          ctx.commit(CartStoreMutationTypes.SET_CART_STATE, cartState);
        } else throw new Error('Error');
      } catch (error) {
        console.log(error);
      } finally {
        ctx.commit(CartStoreMutationTypes.SET_IS_LOADING, false);
      }
    },
    async fetchCart(ctx): Promise<void> {
      try {
        const res = await fetch('/cart.js');
        if (res.status === 200) {
          const cart = await res.json() as ICart;
          ctx.commit(CartStoreMutationTypes.SET_CART_COUNT, cart.item_count);
        } else throw new Error();
      } catch (error) {
        console.log(error);
      }
    },
  },
});

export function useStore(): Store<State> {
  return store;
}
