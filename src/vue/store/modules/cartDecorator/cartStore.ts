import {
  Module,
  VuexModule,
  Mutation,
  Action,
  getModule,
} from 'vuex-module-decorators';
import store from 'Vue/store';
import { ILineItem } from 'Types/shopify/common.type';
import { ICart } from 'Types/shopify/cart.type';

export type CartItems = {
  items: ILineItem[]
};

export enum CartStoreMutationTypes {
  SET_CART_STATE = 'SET_CART_STATE',
  SET_IS_LOADING = 'SET_IS_LOADING',
}

type CartPayload = {
  id: number | string,
  quantity: number,
};

@Module({ name: 'CartModule', dynamic: true, store })
class CartStore extends VuexModule {
  cartItems: CartItems = { items: [] };

  isLoading: boolean;

  get getCartItems(): CartItems {
    return this.cartItems;
  }

  get getIsLoading(): boolean {
    return this.isLoading;
  }

  @Mutation
  [CartStoreMutationTypes.SET_CART_STATE](cartState: CartItems): void {
    this.cartItems = cartState;
  }

  @Mutation
  [CartStoreMutationTypes.SET_IS_LOADING](isLoading: boolean): void {
    this.isLoading = isLoading;
  }

  @Action
  addCartState(cartState: CartItems): void {
    this.context.commit(CartStoreMutationTypes.SET_CART_STATE, cartState);
  }

  @Action
  async updateCartState(payload: CartPayload): Promise<void> {
    const { id, quantity } = payload;
    const formData = {
      id,
      quantity,
    };

    try {
      this.context.commit(CartStoreMutationTypes.SET_IS_LOADING, true);
      const res = await fetch('/cart/change.js', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(formData),
      });

      if (res.status === 200) {
        const cartState = await res.json() as ICart;
        const cartItem = { items: cartState.items.filter((item) => item.key === id) as ILineItem[] };

        this.context.commit(CartStoreMutationTypes.SET_CART_STATE, cartItem);
      } else throw new Error('Error');
    } catch (error) {
      console.log(error);
    } finally {
      this.context.commit(CartStoreMutationTypes.SET_IS_LOADING, false);
    }
  }
}

export default getModule(CartStore);
