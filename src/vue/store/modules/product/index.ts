import {
  Module,
  VuexModule,
  Mutation,
  getModule,
  Action,
} from 'vuex-module-decorators';
import store from 'Vue/store';

export enum MutationType {
  setAvailable = 'SET_AVAILABLE',
  setVariantId = 'SET_VARIANT_ID',
  setQuantity = 'SET_QUANTITY',
  setCurrentPrice = 'SET_CURRENT_PRICE',
}

@Module({ name: 'ProductStore', dynamic: true, store })
class ProductStore extends VuexModule {
  private available: boolean;

  private variantId: number;

  private quantity: number;

  private currentPrice: number;

  get getAvailable(): boolean {
    return this.available;
  }

  get getVariantId(): number {
    return this.variantId;
  }

  get getQuantity(): number {
    return this.quantity;
  }

  get getCurrentPrice(): number {
    return this.currentPrice;
  }

  @Mutation
  [MutationType.setAvailable](payload: boolean): void {
    this.available = payload;
  }

  @Mutation
  [MutationType.setVariantId](payload: number): void {
    this.variantId = payload;
  }

  @Mutation
  [MutationType.setQuantity](payload: number): void {
    this.quantity = payload;
  }

  @Mutation
  [MutationType.setCurrentPrice](payload: number): void {
    this.currentPrice = payload;
  }

  @Action
  setAvailable(payload: boolean): void {
    this.context.commit(MutationType.setAvailable, payload);
  }

  @Action
  setVariantId(payload: number): void {
    this.context.commit(MutationType.setVariantId, payload);
  }

  @Action
  setQuantity(payload: number): void {
    this.context.commit(MutationType.setQuantity, payload);
  }

  @Action
  setCurrentPrice(payload: number): void {
    this.context.commit(MutationType.setCurrentPrice, payload);
  }
}

export default getModule(ProductStore);
