declare module '@shopify/theme-currency' {
  function formatMoney(price: number, type: string): string;
  export { formatMoney };
}

declare module '@shopify/theme-predictive-search' {
  type TYPES = {
    PRODUCT: string,
    PAGE: string,
    ARTICLE: string,
    COLLECTION: string
  };

  type FIELDS = {
    AUTHOR: string,
    BODY: string,
    PRODUCT_TYPE: string,
    TAG: string,
    TITLE: string,
    VARIANTS_BARCODE: string,
    VARIANTS_SKU: string,
    VARIANTS_TITLE: string,
    VENDOR: string
  };

  type UNAVAILABLEPRODUCTS = {
    SHOW: string,
    HIDE: string,
    LAST: string
  };

  class PredictiveSearch {
    config: any;

    static TYPES: TYPES;

    static UNAVAILABLE_PRODUCTS: UNAVAILABLEPRODUCTS;

    static FIELDS: FIELDS;

    constructor(config: any);

    on(eventName: any, callback: any): any;
    off(eventName: any, callback: any): any;
    query(query: string): any;
  }

  // type PredictiveSearch = any;

  export default PredictiveSearch;

}
